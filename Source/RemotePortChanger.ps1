$ListOfServer = $PSScriptRoot + "servers_to_configure.csv"
$RegRdpPath = "HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp\"
$ErrorActionPreference = "stop"



function Test-Credential {
    <#
    .SYNOPSIS
    Short description
    
    .DESCRIPTION
    Long description
    
    .EXAMPLE
    An example
    
    .NOTES
    General notes
    #>
    
    [CmdletBinding()]
    [outputType([bool])]
    param (
        
    )
    
    begin {
        
    }
    
    process {
        
    }
    
    end {
        
    }
}



#Function Working 
function Write-FirewallRule {
    <#
    .SYNOPSIS
    Short description
    
    .DESCRIPTION
    Long description
    
    .PARAMETER DisplayName
    Parameter description
    
    .PARAMETER Direction
    Parameter description
    
    .PARAMETER Protocol
    Parameter description
    
    .PARAMETER Profile
    Parameter description
    
    .PARAMETER PortNumber
    Parameter description
    
    .PARAMETER Action
    Parameter description
    
    .EXAMPLE
    An example
    
    .NOTES
    General notes
    #>
    
    [CmdletBinding()]
    param (
        [parameter(Mandatory, Position = 0)][ValidateSet("Remote Desktop - User Mode (TCP-In)", "Remote Desktop - User Mode (UDP-In)")]$DisplayName,
        [parameter(Mandatory, Position = 1)][ValidateSet('Inbound', 'Outbound')] $Direction,
        [ValidateSet("TCP", "UDP")]$Protocol,
        [ValidateSet('Any', 'Domain', 'Private', 'Public', 'NotApplicable')]$Profile,
        [string]$PortNumber,
        [ValidateSet('NotConfigured', 'Allow', 'Block')]$Action
    )
    
    begin {
        $NewDisplayName = $DisplayName + "_" + $PortNumber 
        $CurrentRule = Get-NetFirewallPortFilter | Where-Object { $_.LocalPort -eq $Port }
        $LocalPort = $CurrentRule.LocalPort
    }
    
    Process {
        if ($LocalPort -eq $PortNumber) {
            Write-Host "Port $PortNumber already define for Rule $($CurrentRule.InstanceID)"
            Write-Host "Details of existing rule "
            $CurrentRule
            
        }
        else {
            try {
                New-NetFirewallRule -Name $NewDisplayName -DisplayName $NewDisplayName -Direction $Direction -Protocol $Protocol -Profile $Profile -LocalPort $PortNumber -Action $Action -ErrorAction stop -ErrorVariable ruleError
            }

            catch {
                Write-Host "Rule $NewDisplayName alreay exist "
                #  Write-Error "$exception"
            }
        }
    }
    end {
        
    }
}



#Working function
function Get-CurrentPort {
    <#
            .SYNOPSIS
            Get-CurrentPort will determine if the destination computer has already configured with desire port.
            .DESCRIPTION
            Get-CurrentPort will determine if the destination computer has already configured with desire port.

            .PARAMETER ComputerName
            Remote computer DNS or IP

            .PARAMETER Port
            New RPD Port Number

            .PARAMETER WinRmPort
            Default winRM port for HTTPS 5986 and for HTTP 5985

            .EXAMPLE
            Get-CurrentPort -ComputerName abc.consoto.com -Port 3389

            .NOTES
            General notes
              #>


    [cmdletBinding()]
    [outputType([String])]
    param (       
        [parameter(Mandatory = $True, Position = 0)][string]$ComputerName,
        [parameter(Mandatory = $True, Position = 1)] [int]$Port,
        [int]$WinRmPort = 5985,
        [switch]$Cred
    )

    begin {
        $CheckPortScriptBlock =
        {
            param($RegPath)
            (Get-ItemProperty -Path $RegPath -Name PortNumber).PortNumber 
        }
        [string]$Result = [string]::Empty
        try {
            $IsComputerReachable = (Test-NetConnection -ComputerName $ComputerName -Port $WinRmPort -ErrorAction $ErrorActionPreference).TcpTestSucceeded
        }
        catch {
            $formatstring = "{0} : {1}`n{2}`n" +
            "    + CategoryInfo          : {3}`n" +
            "    + FullyQualifiedErrorId : {4}`n"
            $fields = $_.InvocationInfo.MyCommand.Name,
            $_.ErrorDetails.Message,
            $_.InvocationInfo.PositionMessage,
            $_.CategoryInfo.ToString(),
            $_.FullyQualifiedErrorId
            $formatstring -f $fields
        }
    }
    process {
        switch ($IsComputerReachable) {
            $True {
                try {
                    $PortStatus = Invoke-Command -ComputerName $ComputerName -ScriptBlock $CheckPortScriptBlock -ArgumentList $RegRdpPath -ErrorAction $ErrorActionPreference
                    if ($PortStatus -eq $Port) {
                        #No need to change port for $ComputerName
                        $Result = "True"
                    }
                    else {
                        #Will change port number to the desire Port for $ComputerName
                        $Result = "False"
                    }
                }
                catch {
                    $Result = "AccessError"
                }
            }
            $false {
                #The remote computer WinRM port is not enable 
                $Result = "WinrmError"
            }
            default { 
                #The CMDLET is not present on host computer
                $Result = "LibraryError"
                # return "Invalid"
            }
        }
    }
    end {
        return $Result
    }    
}



#Get-CurrentPort -ComputerName localhost -Port 3389
#Get-Help Get-CurrentPort

#Function in progress
function New-RDPPort {
    <#
    
    .SYNOPSIS
    
    Create new RDP Port on Remote Machine
    .DESCRIPTION
    Create new RDP port on Remote Machine. In this process the function will try to collect current port number of remote machine and create new firewall rule if Port is not already defined.
    
    .PARAMETER ComputerName
    Remote computer DNS name     
    .PARAMETER PortNumber
    New RDP Port number for remote machine
    
    .PARAMETER Change
    Switch parameter for deciding if we wish to change the port or not    
    .EXAMPLE
    An example
    
    .NOTES
    General notes
    #>
    
    [cmdletBinding()]
    param (
        
        [parameter(ParameterSetName = 'New')][string]$ComputerName,
        [parameter(ParameterSetName = 'New')][int]$PortNumber,
        [switch]$Change
    )
    begin {
        [string]$DoesThisPortAlreadyDefine = Get-CurrentPort -ComputerName $ComputerName -Port $PortNumber 
    }
    process {
        switch ($DoesThisPortAlreadyDefine) {
            "False" {
                Set-ItemProperty -Path $RegRdpPath -Name PortNumber -Value $PortNumber
                Write-FirewallRule -DisplayName 'Remote Desktop - User Mode (TCP-In)' -Direction Inbound -Protocol TCP -Profile Any -PortNumber $PortNumber -Action Allow
                Write-FirewallRule -DisplayName 'Remote Desktop - User Mode (UDP-In)' -Direction Inbound -Protocol UDP -Profile Any -PortNumber $PortNumber -Action  Allow
            }
            "True" {
                Write-Host "Port $PortNumber already Defined in computer $ComputerName"
            }
            "AccessError" {
                New-RDPPort -ComputerName $ComputerName -PortNumber -Change $false
            }
            "WinrmError" {
                Write-Host "WinRm port is not enable on Remtoe computer,  The function can not proceed further "
            }
            default {
                Write-Host "Do Nothing"
            }
        }
    }
    end {
        
    }
}
#Get-Help new-rdpport





