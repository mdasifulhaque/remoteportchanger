# README #

Readme for [Remote port changer](https://bitbucket.org/mdasifulhaque/remoteportchanger) 


### What is this repository for? ###
#### Quick summary: 
This program will allow to change Remote Desktop port using Windows registry and set new Inbound rule to make the port accessible from remote machine.

#### Version
1.0.0.0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

If you find any bug or have any suggestion please send a mail to contact@asifulhaque.info
